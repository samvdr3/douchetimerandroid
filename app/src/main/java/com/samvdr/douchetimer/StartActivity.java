package com.samvdr.douchetimer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.samvdr.douchetimer.util.Type;

public class StartActivity extends AppCompatActivity {
    private Button btnShort;
    private Button btnLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        fetchWidgets();
        addListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchWidgets() {
        btnShort = findViewById(R.id.btnShort);
        btnLong = findViewById(R.id.btnLong);
    }

    private void addListeners() {
        btnShort.setOnClickListener(v -> onShortClicked(v));
        btnLong.setOnClickListener(v -> onLonglicked(v));
    }

    private void onShortClicked(View v) {
        Intent i = new Intent(this, TimerActivity.class);
        i.putExtra("showerType", Type.SHORT);
        startActivity(i);
    }

    private void onLonglicked(View v) {
        Intent i = new Intent(this, TimerActivity.class);
        i.putExtra("showerType", Type.LONG);
        startActivity(i);
    }
}
