package com.samvdr.douchetimer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.samvdr.douchetimer.util.Settings;

public class SettingsActivity extends AppCompatActivity {
    private EditText inShortShower;
    private EditText inLongShower;
    private EditText inTimeBetweenAlarms;
    private Button btnSave;
    private TextView txtAbout;
    private Switch swtchAutoStart;

    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settings = Settings.getInstance(this);
        fetchWidgers();
        setText();
        addListeners();
    }

    @Override
    public void onBackPressed() {
        saveSettings();
        Toast.makeText(this, "Settings are saved", Toast.LENGTH_LONG).show();

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        saveSettings();
        Toast.makeText(this, "Settings are saved", Toast.LENGTH_LONG).show();

        super.onDestroy();
    }

    private void saveSettings() {
        settings.setShortShower(Double.parseDouble(inShortShower.getText().toString()));
        settings.setLongShower(Double.parseDouble(inLongShower.getText().toString()));
        settings.setTimeBetweenAlarms(Double.parseDouble(inTimeBetweenAlarms.getText().toString()));
        settings.setAutoStart(swtchAutoStart.isChecked());
        settings.saveJson();

    }

    private void fetchWidgers() {
        inShortShower = findViewById(R.id.inTimeShort);
        inLongShower = findViewById(R.id.inTimeLong);
        inTimeBetweenAlarms = findViewById(R.id.inTimeBetween);
        btnSave = findViewById(R.id.btnSave);
        txtAbout = findViewById(R.id.txtAbout);
        swtchAutoStart = findViewById(R.id.swtchAutoStart);
    }

    private void setText() {
        inShortShower.setText(String.format("%.0f", settings.getShortShower()));
        inLongShower.setText(String.format("%.0f", settings.getLongShower()));
        inTimeBetweenAlarms.setText(String.format("%.0f", settings.getTimeBetweenAlarms()));
        swtchAutoStart.setChecked(settings.isAutoStart());
    }

    private void addListeners() {
        btnSave.setOnClickListener(v -> saveSettings());
        txtAbout.setOnClickListener(v -> onAboutClicked(v));
    }

    private void onAboutClicked(View v) {
    }
}
