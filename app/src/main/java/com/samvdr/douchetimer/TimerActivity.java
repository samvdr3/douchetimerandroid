package com.samvdr.douchetimer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.samvdr.douchetimer.util.Settings;
import com.samvdr.douchetimer.util.Type;

import java.io.IOException;

// https://android--code.blogspot.com/2017/07/android-play-default-ringtone.html

public class TimerActivity extends AppCompatActivity {
    private TextView txtTimeLeft;
    private Button btnStart;

    private AudioManager audioManager;
    private CountDownTimer countDownTimer;
    private CountDownTimer internalCountDownTimer;
    private Settings settings;
    private Type type;
    private long showerTime;
    private long timeBetweenAlarms;
    private int alarmVolume;
    private boolean clicked;
    private Uri alarnUri;
    private MediaPlayer alarmRingtone;
    private MediaPlayer otherAlarmRingtone;

    private static final String TAG = "ik debug";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        type = (Type) getIntent().getExtras().get("showerType");

        settings = Settings.getInstance(this);
        audioManager = (AudioManager) this.getSystemService(AUDIO_SERVICE);
        clicked = false;
        fetchWidgets();
        btnStart.setOnClickListener(v -> onButtonClicked());
        getSettings();
        updateText(showerTime);

        if (settings.isAutoStart())
            btnStart.callOnClick();
    }

    private void fetchWidgets() {
        txtTimeLeft = findViewById(R.id.txtTimeLeft);
        btnStart = findViewById(R.id.btnStart);
    }

    private void onButtonClicked() {
        if (!clicked) {
            btnStart.setText("Stop");
            clicked = true;
            Log.i(TAG, "onButtonClicked: coundown started");
            countDownTimer = new CountDownTimer(showerTime, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    updateText(millisUntilFinished);
                }

                @Override
                public void onFinish() {
                    updateText(0);
                    Log.i(TAG, "onFinish: countdown finished");
                    onTimerFinished();
                }
            }.start();
        } else {
            Log.i(TAG, "onButtonClicked: countdown stopped");
            countDownTimer.cancel();
            if (otherAlarmRingtone.isPlaying()) {
                otherAlarmRingtone.stop();
            }
            if (alarmRingtone.isPlaying()) {
                alarmRingtone.stop();
            }
            if (internalCountDownTimer != null) {
                internalCountDownTimer.cancel();
            }
            btnStart.setText("Start");
            alarmRingtone.stop();
            clicked = false;
            updateText(showerTime);
            getSettings();
        }
    }

    private void onTimerFinished() {
        alarmRingtone.start();
        try {
            Thread.sleep(5000);
            alarmRingtone.stop();
            alarmRingtone.reset();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        internalCountDownTimer = new CountDownTimer(timeBetweenAlarms, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateText(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                updateText(0);
                otherAlarmRingtone.start();
            }
        }.start();
    }

    private void getSettings() {
        switch (type) {
            case SHORT:
                showerTime = (long) (settings.getShortShower() * 60000);
                break;
            case LONG:
                showerTime = (long) (settings.getLongShower() * 60000);
                break;
        }
        timeBetweenAlarms = (long) (settings.getTimeBetweenAlarms() * 60000);
        alarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        alarnUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

//        Ringtone alarmRingtone = RingtoneManager.getRingtone(getApplicationContext(), alarnUri);
        try {
            alarmRingtone = new MediaPlayer();
            otherAlarmRingtone = new MediaPlayer();
            alarmRingtone.reset();
            alarmRingtone.setDataSource(getApplicationContext(), alarnUri);
            otherAlarmRingtone.reset();
            otherAlarmRingtone.setDataSource(getApplicationContext(), alarnUri);


            alarmRingtone.setAudioStreamType(AudioManager.STREAM_ALARM);
            otherAlarmRingtone.setAudioStreamType(AudioManager.STREAM_ALARM);

            alarmRingtone.prepare();
            otherAlarmRingtone.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateText(long time) {
        txtTimeLeft.setText(String.format("%d:%02d",
                (int) (time/1000)/60,
                (int) (time/1000)%60));
    }
}
