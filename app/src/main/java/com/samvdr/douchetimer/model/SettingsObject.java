package com.samvdr.douchetimer.model;

public class SettingsObject {
    private double longShower;
    private double shortShower;
    private double timeBetweenAlarms;
    private boolean autoStart;

    public SettingsObject() {
    }

    public SettingsObject(double longShower, double shortShower, double timeBetweenAlarms, boolean autoStart) {
        this.longShower = longShower;
        this.shortShower = shortShower;
        this.timeBetweenAlarms = timeBetweenAlarms;
        this.autoStart = autoStart;
    }

    public double getLongShower() {
        return longShower;
    }

    public void setLongShower(double longShower) {
        this.longShower = longShower;
    }

    public double getShortShower() {
        return shortShower;
    }

    public void setShortShower(double shortShower) {
        this.shortShower = shortShower;
    }

    public double getTimeBetweenAlarms() {
        return timeBetweenAlarms;
    }

    public void setTimeBetweenAlarms(double timeBetweenAlarms) {
        this.timeBetweenAlarms = timeBetweenAlarms;
    }

    public boolean isAutoStart() {
        return autoStart;
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }
}
