package com.samvdr.douchetimer.util;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.samvdr.douchetimer.R;
import com.samvdr.douchetimer.model.SettingsObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Settings {
    private static Settings instance;

    private SettingsObject settings;
    private String filename;
    private Context context;
    private Gson gson;

    private Settings(Context context) {
        gson = new Gson();
        this.context = context;
        this.filename = "settings.json";
        readJson();
    }

    public static synchronized Settings getInstance(Context context) {
        if (instance == null) {
            instance = new Settings(context);
        }
        return instance;
    }

    private void readJson() {
        String json;

        try {
            FileInputStream inputStream = context.openFileInput(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer);
            settings = gson.fromJson(json, SettingsObject.class);
        } catch (IOException e) {
            settings = new SettingsObject(
                    20, 10,
                    1, false);
            saveJson();
            Toast.makeText(context, context.getString(R.string.toastText), Toast.LENGTH_LONG).show();
        }
    }

    public double getLongShower() {
        return settings.getLongShower();
    }

    public void setLongShower(double longShower) {
        settings.setLongShower(longShower);
    }

    public double getShortShower() {
        return settings.getShortShower();
    }

    public void setShortShower(double shortShower) {
        settings.setShortShower(shortShower);
    }

    public double getTimeBetweenAlarms() {
        return settings.getTimeBetweenAlarms();
    }

    public void setTimeBetweenAlarms(double timeBetweenAlarms) {
        settings.setTimeBetweenAlarms(timeBetweenAlarms);
    }

    public boolean isAutoStart() {
        return settings.isAutoStart();
    }

    public void setAutoStart(boolean autoStart) {
        settings.setAutoStart(autoStart);
    }

    public void saveJson() {
        try {
            FileOutputStream outputStream = context.openFileOutput(filename, context.MODE_PRIVATE);
            outputStream.write(gson.toJson(settings).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
